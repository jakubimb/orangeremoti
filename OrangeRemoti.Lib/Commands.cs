﻿using System;

namespace OrangeRemoti.Lib
{
    /// <summary>
    /// Orange TV box remote commands
    /// Resource: https://communaute.orange.fr/t5/TV-par-ADSL-et-Fibre/API-pour-commander-le-decodeur-TV-depusi-une-tablette/td-p/43443/page/2
    /// </summary>
    public class Commands
    {
        /// <summary>
        /// Key interaction modes
        /// </summary>
        public class Modes
        {
            public const int SHORT_CLICK = 0;
            public const int LONG_CLICK = 1;
            public const int RELEASE_AFTER_LONG_CLICK = 2;
        }

        /// <summary>
        /// Key codes
        /// </summary>
        public class Codes
        {
            public const int ON_OFF = 116;

            public const int NUM_0 = 512;
            public const int NUM_1 = 513;
            public const int NUM_2 = 514;
            public const int NUM_3 = 515;
            public const int NUM_4 = 516;
            public const int NUM_5 = 517;
            public const int NUM_6 = 518;
            public const int NUM_7 = 519;
            public const int NUM_8 = 520;
            public const int NUM_9 = 521;

            public const int CHANNEL_UP = 402;
            public const int CHANNEL_DOWN = 403;

            public const int VOL_UP = 115;
            public const int VOL_DOWN = 114;
            public const int MUTE = 113;

            public const int UP = 103;
            public const int DOWN = 108;
            public const int LEFT = 105;
            public const int RIGHT = 116;

            public const int OK = 352;
            public const int BACK = 158;
            public const int MENU = 139;
            public const int PLAY_PAUSE = 164;
            public const int FBWD = 168;
            public const int FFWD = 159;
            public const int REC = 167;
            public const int VOD = 393;
        }
    }
}
